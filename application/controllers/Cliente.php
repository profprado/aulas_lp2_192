<?php 

// um controlador é um gerador de páginas.
class Cliente extends CI_Controller{

    public function index(){
        $this->load->view('common/header');
        $this->load->view('mdb'); 
        $this->load->view('common/footer');
    }

    public function lista(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('cliente/topo');
        $this->load->view('cliente/tabela');
        $this->load->view('cliente/alerta');
        $this->load->view('cliente/opcoes');
        $this->load->view('componente/rodape');
        $this->load->view('common/footer');
    }

    // cada página de um controlador é criada por uma função
    public function cadastro(){
        $this->load->view('cliente/topo');
    }

    public function compras(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('componente/jumbotron');
        $this->load->view('componente/descricao');
        $this->load->view('componente/linha1');
        $this->load->view('componente/linha2');
        $this->load->view('componente/linha3');
        $this->load->view('componente/pagination');
        $this->load->view('componente/rodape');
        $this->load->view('common/footer');
    }
}